<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'chaussure' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~q!?y:+7epFMh*K@Zw(vhXZ9@j^ek%UM@;c:ULqZ0O1DzhJkTS|Q254-,]DJx$5f' );
define( 'SECURE_AUTH_KEY',  'Km m+k49n3:pdSpkhOP]Ww6VFIJrq>.3(Z.(P[.&~W6uO>[](`=[4*l&H`42(ENu' );
define( 'LOGGED_IN_KEY',    'Qdb?7rCp /5c4M<dLt3S$ie<loL3m^>ETo1:*= !5A_0e]bOGdog,D+J%G.IDHS?' );
define( 'NONCE_KEY',        'P<4O7E~(vp.}iq;o1 G>rL&dmZ8W1HR,4RiyHV@Gx]b4!@3Ro9YjiPa9R{Q<C|)Z' );
define( 'AUTH_SALT',        '5_?9l<Bq2a#L^UxvjSv7WJ#]@,Wl[>7*%9/8CQq7v0sZY$,Z[zAHjohMuk-+Gk?&' );
define( 'SECURE_AUTH_SALT', 'N,Srn^5E29AHw$`E/w|&ZK:1~}-J=E9|s7lQ]6I 4X`Iu!8jYYiUj,4+`-1S.Y)^' );
define( 'LOGGED_IN_SALT',   'eEu3DvA%j0971CE})tRxnY*=/I]@$_7/cQ}!1fei|3uFjmKKGR*79U5MWNA;A8Q7' );
define( 'NONCE_SALT',       'iy}So99E[<(>L{r{;b^S_kv;q39i#e$ra>G4pQ&^8}ZnT3wk1oUcN@MB7_+Hz!tN' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
